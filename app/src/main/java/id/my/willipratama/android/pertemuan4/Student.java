package id.my.willipratama.android.pertemuan4;

import android.os.Parcel;
import android.os.Parcelable;

public class Student implements Parcelable {
    protected Student(Parcel in) {
        name = in.readString();
        nim = in.readString();
        email = in.readString();
        phone = in.readString();
        year_entry = in.readInt();
        avatar = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(nim);
        dest.writeString(email);
        dest.writeString(phone);
        dest.writeInt(year_entry);
        dest.writeInt(avatar);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Student> CREATOR = new Creator<Student>() {
        @Override
        public Student createFromParcel(Parcel in) {
            return new Student(in);
        }

        @Override
        public Student[] newArray(int size) {
            return new Student[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getYear_entry() {
        return year_entry;
    }

    public void setYear_entry(int year_entry) {
        this.year_entry = year_entry;
    }

    public int getAvatar() {
        return avatar;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }

    private String name, nim, email, phone;
    private int year_entry, avatar;

    public Student() {
    }
}
