package id.my.willipratama.android.pertemuan4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MoveWithObjectActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String EXTRA_STUDENT = "extra_student";
    public String phoneNumber = "phone_number";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_move_with_object);

        ImageView ivAvatar = findViewById(R.id.iv_avatar);
        TextView tvObject = findViewById(R.id.tv_object_received);

        Student student = getIntent().getParcelableExtra(EXTRA_STUDENT);
        String text = "Nama: " + student.getName() +
                " (Angkatan " + student.getYear_entry() + ")"
                + "\nNIM: " + student.getNim()
                + "\nE-Mail: " + student.getEmail()
                + "\nTel: " + student.getPhone();
        tvObject.setText(text);
        ivAvatar.setImageResource(student.getAvatar());

        Button btnDialPhone = findViewById(R.id.btn_dial_number);
        btnDialPhone.setOnClickListener(this);
        phoneNumber = student.getPhone();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_dial_number) {
            Intent dialPhoneIntent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneNumber));
            startActivity(dialPhoneIntent);
        }
    }
}