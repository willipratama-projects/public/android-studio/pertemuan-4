package id.my.willipratama.android.pertemuan4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnMoveActivity = findViewById(R.id.btn_move_activity);
        btnMoveActivity.setOnClickListener(this);

        Button btnMoveWithDataActivity = findViewById(R.id.btn_move_activity_data);
        btnMoveWithDataActivity.setOnClickListener(this);

        Button btnMoveWithObject = findViewById(R.id.btn_move_activity_object);
        btnMoveWithObject.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_move_activity) {
            Intent moveIntent = new Intent(MainActivity.this, MoveActivity.class);
            startActivity(moveIntent);
        } else if (view.getId() == R.id.btn_move_activity_data) {
            Intent moveWithDataIntent = new Intent(MainActivity.this, MoveWithDataActivity.class);
            moveWithDataIntent.putExtra(MoveWithDataActivity.EXTRA_NAME, "Willi Pratama");
            moveWithDataIntent.putExtra(MoveWithDataActivity.EXTRA_NIM, "123456789");
            moveWithDataIntent.putExtra(MoveWithDataActivity.EXTRA_YEAR_ENTRY, 2021);
            startActivity(moveWithDataIntent);
        } else if (view.getId() == R.id.btn_move_activity_object) {
            Student student = new Student();

            student.setName("Willi Pratama");
            student.setNim("123456789");
            student.setEmail("willipratama@buminusantara.ac.id");
            student.setPhone("+62 856-2447-9449");
            student.setYear_entry(2021);
            student.setAvatar(R.drawable.avatar);

            Intent moveWithObjectIntent = new Intent(MainActivity.this, MoveWithObjectActivity.class);
            moveWithObjectIntent.putExtra(MoveWithObjectActivity.EXTRA_STUDENT, student);
            startActivity(moveWithObjectIntent);
        }
    }
}