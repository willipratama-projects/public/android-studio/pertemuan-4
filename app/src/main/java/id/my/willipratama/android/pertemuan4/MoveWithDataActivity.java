package id.my.willipratama.android.pertemuan4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class MoveWithDataActivity extends AppCompatActivity {
    public static final String EXTRA_NAME = "extra_name";
    public static final String EXTRA_NIM = "extra_nim";
    public static final String EXTRA_YEAR_ENTRY = "extra_year_entry";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_move_with_data);

        TextView tvDataReceived = findViewById(R.id.tv_data_received);

        String name = getIntent().getStringExtra(EXTRA_NAME);
        String nim = getIntent().getStringExtra(EXTRA_NIM);
        int year_entry = getIntent().getIntExtra(EXTRA_YEAR_ENTRY, 0);

        String text = "Nama: " + name + " (" + nim + ") , Tahun Masuk: " + year_entry;
        tvDataReceived.setText(text);
    }
}